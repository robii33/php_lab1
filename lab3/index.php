

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=500">

    <title>Calculator</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php 


    try {
        if ((isset($_POST)) && (!empty($_POST["input_field"]))) {
            // $result = eval('return ' . $_POST["input_field"] . ';');
            $eq = $_POST["input_field"];
            
            if(preg_match('/([\d\.\s]+)([\+\-\*\/])(\-?[\d\.\s]+)/', $eq, $matches) !== FALSE){
                $operator = $matches[2];

                switch($operator){
                    case '+':
                        $p = $matches[1] + $matches[3];
                        break;
                    case '-':
                        $p = $matches[1] - $matches[3];
                        break;
                    case '*':
                        $p = $matches[1] * $matches[3];
                        break;
                    case '/':
                        $p = $matches[1] / $matches[3];
                        break;
                }

                $result = $p;
            }

        } else {
            $result = '';
        }
    } catch (ErrorException $p){

        $result = '';
    }
    ?>
    <header class="header"><h1 class="header__title">PHP Calculator</h1></header>
    <main>
        
        <div class="wrapper calculator">
            <form action="index.php" method="post" id="form">
                <div class="calculator__body">
                    <div class="input-wrapper">
                        <input name="input_field" type="text" id="input" onkeydown="return false" value="<?php echo $result; ?>">
                    </div>

                    <div class="ouTput">
                        <p><?php $result; ?></p>
                    </div>
                    <div class="action-buttons">
                        <div class="button-wrapper" id="numbers">
                            <button class="button" type="button">7</button>
                            <button class="button" type="button">8</button>
                            <button class="button" type="button">9</button>
                            <button class="button" type="button">4</button>
                            <button class="button" type="button">5</button>
                            <button class="button" type="button">6</button>
                            <button class="button" type="button">1</button>
                            <button class="button" type="button">2</button>
                            <button class="button" type="button">3</button>
                            <button class="button" type="button">0</button>
                            
                        </div>
                        <div class="button-wrapper operator" id="operators">
                            <button class="button" type="button">+</button>
                            <button class="button" type="button">-</button>
                            <button class="button" type="button">*</button>
                            <button class="button" type="button">/</button>
                            <button class="button" type="button">(</button>
                            <button class="button" type="button">)</button>
                            <button class="button" type="button" onclick="resetF()">C</button>
                            <button class="button" type="button" onclick="submitF()">=</button>
                        </div>
                    </div>
                    
                </div>
            </form>

            

        </div>
        
    </main>

    <script src="js/script.js"></script>
</body>
</html>
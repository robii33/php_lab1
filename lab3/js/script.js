function assignFunction(button) {
    console.log(button.textContent);

    const operators = {'+':'+', '-':'-', '*': '*', '/':'/'}
    let input = document.getElementById('input').value.slice(-1);

    if (((input != (button.textContent) && (!(input in operators)))) || (!isNaN(parseFloat(button.textContent)) && isFinite(button.textContent)) || (button.textContent in {'(': '(', ')':')'})) {
    
        let str = document.getElementById('input').value + button.textContent;
        
        document.getElementById('input').value = str;

    }
    
}

function submitF() {
    console.log(11)
    const operators = {'+':'+', '-':'-', '*': '*', '/':'/'};
    if (!(document.getElementById('input').value.slice(-1) in operators)) {
        document.getElementById('form').submit()
    }
}

function resetF() {
    document.getElementById('input').value = '';
}


let buttons = document.getElementById('numbers');

for (let i = 0; i < buttons.children.length; i++) {
    let button = buttons.children[i];
    button.addEventListener("click", function(){assignFunction(button)});
}

let operators = document.getElementById("operators")
for (let i = 0; i < (operators.children.length - 2); i++) {
    let button = operators.children[i];
    button.addEventListener("click", function(){assignFunction(button)});
}

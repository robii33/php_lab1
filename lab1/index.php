<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php
        $text = "Hello, world. This is my first php lab!";
    ?>

    <header>
        <img class="polytech" src="img/Polykek.jpg" alt="moscow polytech" width="200">
        <h1 class="title">Hello, World!</h1>
    </header>

    <main class="main">
        <p><?php echo $text ?></p>
    </main>

    <footer>
        <p>Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг.</p>
    </footer>
</body>
</html>
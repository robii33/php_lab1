<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab2</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header class="header">
        <img class="logo" src="img/Polykek.jpg" width="100">
        <h1 class="title">Feedback form</h1>
    </header>

    <main class="main">
        <form class="form" action="http://httpbin.org/post" method="post">
            <div class="input-wrapper">
                <label for="name">Имя</label>
                <input type="text" id="name" name="name">
            </div>

            <div class="input-wrapper">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="e-mail">
            </div>

            <div class="input-wrapper">
                <label for="complaint">Жалоба</label>
                <input type="radio" id="complaint" name="type" value="complaint">

                <label for="offer">Предложение</label>
                <input type="radio" id="offer" name="type" value="offer">

                <label for="gratitude">Благодарность</label>
                <input type="radio" id="gratitude" name="type" value="gratitude">
            </div>

            <div class="input-wrapper">
                <label for="text">Текст обращения</label>
                <input type="text" id="text" name="text"></input>
            </div>

            <div class="input-wrapper">
                <p>Вариант ответа</p>
                <label for="sms">СМС</label>
                <input type="checkbox" id="sms" name="answer" value="sms">

                <label for="email-answer">E-mail</label>
                <input type="checkbox" id="email-answer" name="answer" value="email">

            </div>

            <button type="submit">Отправить</button>
            <a class="link" href="second.php">2 страница</a>
            
        </form>


    </main>
</body>
</html>
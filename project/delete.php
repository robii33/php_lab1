<?php
    $my_db = mysqli_connect('localhost', 'root', '', 'project');
    if (mysqli_connect_errno()) echo 'Ошибка подключения к БД'. mysqli_connect_error();
    $sql = 'SELECT `id`, `surname`, LEFT(`name`, 1) as `name` FROM `notebook`';
    $sql_res = mysqli_query($my_db, $sql);
    if (mysqli_errno($my_db)) echo 'Ошибка запроса';

    while($row = mysqli_fetch_assoc($sql_res)){
        echo '<div class="div-edit">';
        echo '<a href="?id='.$row['id'].'&p=delete">'.$row['surname'].' '.$row['name'].'.</a></div>';
    }

    if ($_GET['p'] == 'delete' and isset($_GET['id'])) {
        echo 'Вы действительно хотите удалить данный элемент?';
        echo '<br>';
        echo '<a href="?id='.$_GET['id'].'&p=delete&ans=yes">'.'Да'.'</a>';
    }

    if (isset($_GET['ans']) and $_GET['ans'] == 'yes') {
        $sql = 'DELETE FROM `notebook` WHERE id = '.$_GET['id'];
        $sql_res = mysqli_query($my_db, $sql);
        if (mysqli_errno($my_db)) echo 'Ошибка запроса';
        echo '<br>';
        echo 'Успешно удалено';
        header("Refresh:2; url=index.php?p=delete");
    }
?>